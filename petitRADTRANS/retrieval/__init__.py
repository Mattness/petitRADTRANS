# TODO this should be blank (authorship is ensured with Git and using import shortcuts is bad practice)
__author__ = "Evert Nasedkin"
__copyright__ = "Copyright 2021, Evert Nasedkin"
__maintainer__ = "Evert Nasedkin"
__email__ = "nasedkinevert@gmail.com"
__status__ = "Development"

from .retrieval import Retrieval
from .retrieval_config import RetrievalConfig
